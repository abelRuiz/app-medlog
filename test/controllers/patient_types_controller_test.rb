require 'test_helper'

class PatientTypesControllerTest < ActionController::TestCase
  setup do
    @patient_type = patient_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patient_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create patient_type" do
    assert_difference('PatientType.count') do
      post :create, patient_type: { string: @patient_type.string }
    end

    assert_redirected_to patient_type_path(assigns(:patient_type))
  end

  test "should show patient_type" do
    get :show, id: @patient_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @patient_type
    assert_response :success
  end

  test "should update patient_type" do
    patch :update, id: @patient_type, patient_type: { string: @patient_type.string }
    assert_redirected_to patient_type_path(assigns(:patient_type))
  end

  test "should destroy patient_type" do
    assert_difference('PatientType.count', -1) do
      delete :destroy, id: @patient_type
    end

    assert_redirected_to patient_types_path
  end
end
