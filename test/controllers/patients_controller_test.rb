require 'test_helper'

class PatientsControllerTest < ActionController::TestCase
  setup do
    @patient = patients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create patient" do
    assert_difference('Patient.count') do
      post :create, patient: { address_id: @patient.address_id, birth_date: @patient.birth_date, birthplace: @patient.birthplace, cell_phone: @patient.cell_phone, first_name: @patient.first_name, gender: @patient.gender, last_name: @patient.last_name, occupation: @patient.occupation, patient_type_id: @patient.patient_type_id, phone: @patient.phone, rfc: @patient.rfc, status: @patient.status, user_id: @patient.user_id }
    end

    assert_redirected_to patient_path(assigns(:patient))
  end

  test "should show patient" do
    get :show, id: @patient
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @patient
    assert_response :success
  end

  test "should update patient" do
    patch :update, id: @patient, patient: { address_id: @patient.address_id, birth_date: @patient.birth_date, birthplace: @patient.birthplace, cell_phone: @patient.cell_phone, first_name: @patient.first_name, gender: @patient.gender, last_name: @patient.last_name, occupation: @patient.occupation, patient_type_id: @patient.patient_type_id, phone: @patient.phone, rfc: @patient.rfc, status: @patient.status, user_id: @patient.user_id }
    assert_redirected_to patient_path(assigns(:patient))
  end

  test "should destroy patient" do
    assert_difference('Patient.count', -1) do
      delete :destroy, id: @patient
    end

    assert_redirected_to patients_path
  end
end
