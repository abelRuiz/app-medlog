json.array!(@patient_types) do |patient_type|
  json.extract! patient_type, :id, :string
  json.url patient_type_url(patient_type, format: :json)
end
