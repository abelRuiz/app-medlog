json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :last_name, :gender, :birth_date, :phone, :cell_phone, :rfc, :birthplace, :occupation, :status, :patient_type_id, :user_id, :address_id
  json.url patient_url(patient, format: :json)
end
