json.array!(@tutors) do |tutor|
  json.extract! tutor, :id, :first_name, :last_name, :occupation, :rfc, :phone, :cell_phone
  json.url tutor_url(tutor, format: :json)
end
