module ApplicationHelper

	def get_controller_herlper
  		Rails.application.routes.router.recognize(request) do |route, matches, param|
		  return matches[:controller]
		end
  	end

  	def active_option_nav(path)
  		hash = { 'welcome' => 'welcome', 'patients' => 'patients', 'medical_consultations'	=> 'medical_consultations'}
  		route = get_controller_herlper
	    hash.each do |key, value|
	    	if key == path and value == route 
	    		return "active"
	    		break
	    	else
	    	end
	    end
  	end

end
