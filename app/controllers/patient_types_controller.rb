#encoding: utf-8
class PatientTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_patient_type, only: [:show, :edit, :update, :destroy]

  # GET /patient_types
  # GET /patient_types.json
  def index
    @patient_types = PatientType.paginate(:page => params[:page], :per_page => 15)
  end

  # GET /patient_types/1
  # GET /patient_types/1.json
  def show
  end

  # GET /patient_types/new
  def new
    @patient_type = PatientType.new
  end

  # GET /patient_types/1/edit
  def edit
  end

  # POST /patient_types
  # POST /patient_types.json
  def create
    @patient_type = PatientType.new(patient_type_params)

    respond_to do |format|
      if @patient_type.save
        format.html { redirect_to @patient_type, notice: 'El registro se ha creado correctamente.' }
        format.json { render :show, status: :created, location: @patient_type }
      else
        format.html { render :new }
        format.json { render json: @patient_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patient_types/1
  # PATCH/PUT /patient_types/1.json
  def update
    respond_to do |format|
      if @patient_type.update(patient_type_params)
        format.html { redirect_to @patient_type, notice: 'El registro se ha actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @patient_type }
      else
        format.html { render :edit }
        format.json { render json: @patient_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patient_types/1
  # DELETE /patient_types/1.json
  def destroy
    @patient_type.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'El registro fue destruido con éxito' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient_type
      @patient_type = PatientType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_type_params
      params.require(:patient_type).permit(:name)
    end
end
