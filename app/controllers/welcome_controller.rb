class WelcomeController < ApplicationController
 	before_action :authenticate_user!
  
  	def index
	  	@patient_count = Patient.count
	  	@medical_consultation_count = MedicalConsultation.count
	  	@patient_types_count = PatientType.count
 	end
end
