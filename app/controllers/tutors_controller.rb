#encoding: utf-8
class TutorsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_tutor, only: [:show, :edit, :update, :destroy]

  # GET /tutors
  # GET /tutors.json
  def index
    @tutors = Tutor.all
  end

  # GET /tutors/1
  # GET /tutors/1.json
  def show
  end

  # GET /tutors/new
  def new
    @tutor = Tutor.new
    @patient = Patient.find(params[:patient_id])
  end

  # GET /tutors/1/edit
  def edit
    @patient = Patient.find(params[:patient_id])
  end

  # POST /tutors
  # POST /tutors.json
  def create
    @tutor = Tutor.new(tutor_params)
    @patient = Patient.find(params[:patient_id])
    @tutor.patient = @patient.id

    respond_to do |format|
      if @tutor.save
        format.html { redirect_to patient_path(@patient, anchor: "tab_tutors_patient"), notice: 'Tutor agregado correctamente.' }
        format.json { render :show, status: :created, location: @tutor }
      else
        format.html { render :new }
        format.json { render json: @tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tutors/1
  # PATCH/PUT /tutors/1.json
  def update
    @patient = Patient.find(params[:patient_id])
    respond_to do |format|
      if @tutor.update(tutor_params)
        format.html { redirect_to patient_path(@patient, anchor: "tab_tutors_patient"), notice: 'Tutor fue actualizado.' }
        format.json { render :show, status: :ok, location: @tutor }
      else
        format.html { render :edit }
        format.json { render json: @tutor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tutors/1
  # DELETE /tutors/1.json
  def destroy
    @tutor.destroy
    @patient = Patient.find(params[:patient_id])
    respond_to do |format|
      format.html { redirect_to patient_path(@patient, anchor: "tab_tutors_patient"), notice: 'El registro de tutor fue destruido con éxito' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tutor
      @tutor = Tutor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tutor_params
      params.require(:tutor).permit(:first_name, :last_name, :occupation, :rfc, :phone, :cell_phone)
    end
end
