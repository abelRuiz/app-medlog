#encoding: utf-8
class PatientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  #before_action :set_tutor, only: [:show, :edit, :update, :destroy]

  # GET /patients
  # GET /patients.json
  def index
    if params['search']
      search  = params['search']
      @patients = Patient.where("full_name ILIKE ? OR birthplace ILIKE ?","%#{search}%", "%#{search}%").order(last_name: :asc).paginate(:page => params[:page], :per_page => 15)
    else
      @patients = Patient.all.order(last_name: :asc).paginate(:page => params[:page], :per_page => 15)
    end
    #@patients = Patient.all.paginate(:page => params[:page], :per_page => 8)
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @tutors = Tutor.where(patient: @patient.id)
    @medical_consultations = MedicalConsultation::where(patient: @patient.id)
  end

  # GET /patients/new
  def new
    #@tutor          = Tutor.new
    @patient        = Patient.new
    @countries      = Country.all
    @cities         = []#City.all
    @patient_types  = PatientType.all
  end

  # GET /patients/1/edit
  def edit
    @countries      = Country.all
    @cities         = City.where(country_id: @patient.country_id) #country del patient
    @patient_types  = PatientType.all
    @tutors = Tutor.where(patient: @patient.id)
    @medical_consultations = MedicalConsultation::where(patient: @patient.id)

    #if @patient.tutor
    #  @tutor        = Tutor.find(@patient.tutor.id)
    #else
    #  @tutor        = Tutor.new
    #end
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient  = Patient.new(patient_params)
    @patient.full_name = get_full_name

    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'El registro se ha creado correctamente' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    @patient.full_name = get_full_name
    
    respond_to do |format|

      if @patient.update(patient_params)
        format.html { redirect_to @patient, notice: 'El registro se ha actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end

      #if @tutor.update(tutor_params)
      #  format.html { redirect_to @patient, notice: 'Patient Tutor was successfully updated.' }
      #  format.json { render :show, status: :ok, location: @patient }
      #else
      #  format.html { render :edit }
      #  format.json { render json: @patient.errors, status: :unprocessable_entity }
      #end

    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'El registro fue destruido con éxito' }
      format.json { head :no_content }
    end
  end

  # GET /alumno_tutors/search_alumno
  def search_patient
    full_name   = params[:full_name] == nil ? '' : params[:full_name]
    @patients     = Patient.where("full_name ILIKE '%#{full_name}%'")
    render "patients/_search_patients", :layout => false
  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    def get_full_name
      return "#{params[:patient][:first_name]} #{params[:patient][:last_name]}"
    end

    #def set_tutor
    #  @tutor   = Tutor.find(@patient.tutor.id)
    #end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:first_name, :last_name, :gender, :city_id, :country_id, :birth_date, :birthplace, :street, :colony, :number, :postal_code,:fiscal_street, :fiscal_colony, :fiscal_number, :fiscal_postal_code, :patient_type_id, :user_id)
    end

    #def tutor_params
    #  params.require(:tutor).permit(:first_name, :last_name, :occupation, :rfc, :phone, :cell_phone)
    #end
end
