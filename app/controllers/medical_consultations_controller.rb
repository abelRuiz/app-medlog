#encoding: utf-8
class MedicalConsultationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_medical_consultation, only: [:show, :edit, :update, :destroy]

  # GET /medical_consultations
  # GET /medical_consultations.json
  def index
    #if params['search']
    #  search  = params['search']
    #  @medical_consultations = MedicalConsultation.where("full_name ILIKE ? OR birthplace ILIKE ?","%#{search}%", "%#{search}%").paginate(:page => params[:page], :per_page => 8)
    #else
      @medical_consultations = MedicalConsultation.order(id: :desc).all.paginate(:page => params[:page], :per_page => 15)
    #end
  end

  # GET /medical_consultations/1
  # GET /medical_consultations/1.json
  def show
    @medical_consultations = nil
    if @medical_consultation.patient_id
      @patient = Patient.find(@medical_consultation.patient_id)
      @medical_consultations = MedicalConsultation.order(id: :desc).where(patient_id: @patient.id)#.where.not(id: @medical_consultation.id)
    end
  end

  # GET /medical_consultations/new
  def new
    if params[:patient_id]
      @patient = Patient.find(params[:patient_id])
      if @patient.id
        @patient_medical_consultations = MedicalConsultation.order(id: :desc).where(patient_id: @patient.id)
      end     
    else
      @patient = Patient.new
    end
    @medical_consultation = MedicalConsultation.new
  end

  # GET /medical_consultations/1/edit
  def edit
    begin
      @patient = Patient.find(@medical_consultation.patient_id)
      if @patient.id
        @patient_medical_consultations = MedicalConsultation.order(id: :desc).where(patient_id: @patient.id)
      end 
    rescue ActiveRecord::RecordNotFound => e
      @patient = Patient.new
    end
  end

  # POST /medical_consultations
  # POST /medical_consultations.json
  def create
    @medical_consultation = MedicalConsultation.new(medical_consultation_params)
    @medical_consultation.user = current_user
    respond_to do |format|
      if @medical_consultation.save
        format.html { redirect_to @medical_consultation, notice: 'El registro se ha creado correctamente.' }
        format.json { render :show, status: :created, location: @medical_consultation }
      else
        format.html { render :new }
        format.json { render json: @medical_consultation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medical_consultations/1
  # PATCH/PUT /medical_consultations/1.json
  def update
    @medical_consultation.user = current_user
    respond_to do |format|
      if @medical_consultation.update(medical_consultation_params)
        format.html { redirect_to @medical_consultation, notice: 'El registro se ha actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @medical_consultation }
      else
        format.html { render :edit }
        format.json { render json: @medical_consultation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medical_consultations/1
  # DELETE /medical_consultations/1.json
  def destroy
    @medical_consultation.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'El registro fue destruido con éxito' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medical_consultation
      @medical_consultation = MedicalConsultation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medical_consultation_params
      params.require(:medical_consultation).permit(:user_id, :patient_id, :endocrine, :endocrine_text, :hypertension, :hypertension_text, :heart_disease, :heart_disease_text, :tuberculosis, :tuberculosis_text, :asthmas_and_allergies, :asthmas_and_allergies_text, :tumor, :tumor_text, :HIV, :HIV_text, :other, :other_text, :prenatal_maternal_age, :prenatal_number_of_deeds, :prenatal_product_deed_number, :prenatal_living, :prenatal_antenatal, :prenatal_complications, :prenatal_which_complications, :natal_natal, :natal_weeks_gestation, :natal_apgar, :natal_weight, :natal_size, :natal_head_circumference, :natal_hospitable, :natal_complications, :postnatal_feeding, :postnatal_supply_amount, :postnatal_number, :postnatal_number, :postnatal_time, :postnatal_reason, :postnatal_weaning_start, :postnatal_weaning_start_type, :postnatal_current_supply, :postnatal_vaccines_bcg, :postnatal_polio, :postnatal_dpt, :postnatal_hib, :postnatal_hepatitis_b, :postnatal_mmr, :postnatal_hepatitis_a, :postnatal_antivaricela, :postnatal_pneumococcus, :postnatal_menarche, :postnatal_rhythm, :neurological_stare, :neurological_sat_dawn, :neurological_rolled, :neurological_path, :neurological_monosyllables, :smoking, :alcoholism, :drug_addiction, :seizures, :asthma, :allergies, :surgical, :transfusions, :hospitalizations, :measles, :rubella, :parotitis, :chickenpox, :congenital_malformations, :medical_history_comments, :neurological_system, :cardiovascular_system, :respiratory_system, :digestive_apparatus, :genitourinary_system, :musculoskeletal, :auditory, :visual, :systems_and_apparatus_comments, :physical_exploration_weight, :physical_exploration_size, :physical_exploration_per_cef, :physical_exploration_temp, :physical_exploration_fc, :physical_exploration_fr, :physical_exploration_ta, :physical_exploration_general_inspection, :physical_exploration_head, :physical_exploration_neck, :physical_exploration_chest, :physical_exploration_abdomen, :physical_exploration_genitals, :physical_exploration_upper_extremities, :physical_exploration_lower_extremities, :physical_exploration_skin, :physical_exploration_comments, :physical_exploration_diagnosis, :physical_exploration_studies_requested, :physical_exploration_treatment, :physical_exploration_prognostic, :interrogation, :diagnosis, :treatment)
    end
end
