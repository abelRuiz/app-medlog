# encoding: utf-8
class MedicalConsultation < ActiveRecord::Base
  belongs_to :user
  belongs_to :patient

  ANSWER_YES_NO = ["---", "SI", "NO"]

  ANSWER_NATAL = ["Parto", "Cesárea", "Fórcep", "Sencillo", "Gemelar", "Multiple"]

  ANSWER_NATAL_HOSPITABLE = ["Medio hispitalario", "Institucional", "Privado", "Otro"]

  ANSWER_POSTNATAL_FEEDING = ["Seno materno", "Fórmula", "Mixto"]

end
