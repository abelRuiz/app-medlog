#encoding: utf-8
class Patient < ActiveRecord::Base
  belongs_to :patient_type
  belongs_to :user
  #belongs_to :address
  belongs_to :city
  belongs_to :tutor
  has_many 	:medical_consultation, dependent: :nullify

  GENDER_TYPES = ["Hombre", "Mujer"]

  def full_name
  	"#{first_name} #{last_name}"
  end

  def age

    if birth_date
      years    = Date.today.year.to_i
      months   = Date.today.month.to_i
      days     = Date.today.day.to_i      

      birth_date_year   = birth_date.strftime('%Y').to_i
      birth_date_month  = birth_date.strftime('%m').to_i
      birth_date_day    = birth_date.strftime('%d').to_i

      if birth_date_month > months && birth_date_day > days
        years = years - birth_date_year - 1
      else
        years = years - birth_date_year
      end

      if birth_date_month > months
        months = months + (12 - birth_date_month)
      else
        months = months - birth_date_month
      end

      year_text   = (years > 1 or years == 0) ? "años" : "año"
      month_text  = (months > 1) ? "meses" : "mes"
      day_text    = (days > 1 ) ? "días" : "día"

      "#{years} #{year_text}, #{months} #{month_text} y #{days} #{day_text}"
    else
      "---"
    end
  end

  def type
    if patient_type
      "#{patient_type.name}"
    else
      "---"
    end
  end 

end
