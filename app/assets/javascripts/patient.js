$(document).ready(function () {

	$('#country_id').on('change', function () {
		var country_id = $(this).attr('selected', 'selected');
		var data = {
			"country_id": country_id.val()
		}
		$.ajax({
			url: '/cities/get_cities_by_country',
		   	data: data,
		   	error: function() {
		   		alert('Error')
		   	},
		   	dataType: 'json',
		   	success: function(r) {
		   		if (r.length) {
		   			$('#citites_select').html('');
		   			for (var i = 0; i < r.length; i++) {
		   				$('#citites_select').append($('<option></option>')
		   				.attr('value', r[i].id)
		   				.text(r[i].name));
		   			};
		   		} else {
		   			$('#citites_select').html('');
		   		}
		   	},
		   	complete: function () {

		   	},
		   	type: 'GET'
		});
	});

	$('#form-search-patient').validate({
	    rules: {
	      full_name: {
	        required: true
	      }
	    },
	    submitHandler: function(form) {
	      var button, text;
	      button = $(form).find('button');
	      text = button.val();

	      $(form).ajaxSubmit({
	        beforeSend: function() {
	          button.val('Search...').prop('disabled', true);
	        },
	        error: function() {},
	        success: function(response) {
		        if (response.length > 0) {
		        	$('.content-search-results').html(response).fadeOut().fadeIn();
				}
	        },
	        complete: function() {
	          return button.val(text).prop('disabled', false);
	        },
	        dataType: 'HTML'
	      });
	    }
  	});

  	$('#form-patient').validate({
	    rules: {
	      "patient[first_name]": {
	        required: true
	      }
	    },
	    submitHandler: function(form) {

	      form.submit();
	    }
  	});

});