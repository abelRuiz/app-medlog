$(document).ready(function () {

	$('.show-content').on('click', function () {
		var content_id = $(this).attr('content-id');
		$(content_id).removeClass('hidden').fadeOut().fadeIn();
	});

	$('body').delegate('.patient-select-data-tabla', 'click', function () {

		$('#content-search-patient').fadeOut();

		var patient_id 			= $(this).attr('patient-id');
		var patient_full_name 	= $(this).attr('patient-full-name');
		// var patient_birth_date 	= $(this).attr('patient-birth-date');
		var patient_birthplace 	= $(this).attr('patient-birthplace');
		var patient_type 		= $(this).attr('patient-type');
		var patient_age 		= $(this).attr('patient-age');

		$('#input-patient-id').val(patient_id);
		$('#input-patient-full-name').val(patient_full_name);
		// $('#input-patient-birth-date').val(patient_birth_date);
		$('#input-patient-birthplace').val(patient_birthplace);
		$('#input-patient-type').val(patient_type);
		$('#input-patient-age').val(patient_age);
		

		$('#content-patient-data').fadeOut().fadeIn();

		$('.form-patient-message').remove();

	});

	$('#form-medical-consultation').validate({
	    rules: {
	      input_patient_full_name: {
	        required: true
	      }
	    },
	    submitHandler: function(form) {
	      var button, text;
	      button = $(form).find('button');
	      text = button.val();

	      form.submit();
	    }
  	});

});