$(document).ready(function () {
  $('#form-tutor').validate({
      rules: {
        "tutor[first_name]": {
          required: true
        },
        "tutor[last_name]": {
          required: true
        }
      },
      submitHandler: function(form) {

        form.submit();
      }
  });
});