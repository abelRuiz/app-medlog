// document: referer

$('.referer').on('click', function() {
    location.href = document.referrer;
});

$(document).ready(function () {
    $("img").addClass("img-responsive");
});

$(".submit-form").on('click',function(){
    alert();
});

(function () {

    'use strict';
    
    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es requerido",
        url: "Por favor introduzca un URL válido. Eje: http://www.google.com",
        email: "Por favor, introduce una dirección de correo electrónico válida."
    });

})();

$('.dropdown-toggle').dropdown();

$(document).ready(function() {
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
});

// document: referer
$('#referer').on('click', function() {
    location.href = document.referrer;
});

// bootbox: alert dialog
function alertBox(type, header, content, buttonText) {

    if( ! buttonText) {
        buttonText = 'Ok';
    }
    
    bootbox.dialog({
        className: 'my-custom-dialog ' + type + '-dialog',
        message: content,
        title: header,
        buttons: {
            main: {
                className: 'btn-' + type + ' btn-sm',
                label: buttonText
            }
        }
    });
}

// bootbox: confirm dialog
function confirmBox(header, content, callback) {
    
    bootbox.dialog({
        className: 'my-custom-dialog warning-dialog',
        message: content,
        title: header,
        buttons: {
            cancel: {
                className: 'btn-default btn-sm',
                label: 'No'
            },
            main: {
                className: 'btn-warning btn-sm',
                label: 'Si',
                callback: callback
            }
        }
    });
}

function masked () {
  $('.masked').inputmask({mask:'(999) 999-9999'});
}

$(function () {
    //Delete A record of any model
    $('.delete-data').on('click', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var anchor = $(this);
        event.preventDefault();
        confirmBox('Eliminar registro', '¿Estas seguro en eliminar el registro?', function() {
            destroyRecord(anchor);
        });
    });
});

function destroyRecord(anchor) {

    var form = anchor.closest('form');
    var text = anchor.text();

    form.submit();
}

$('.masked').inputmask({
    mask:'(999) 999-9999'
});