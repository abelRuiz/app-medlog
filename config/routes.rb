Rails.application.routes.draw do
  
  resources :medical_consultations
  resources :tutors
  resources :cities do
    get 'get_cities_by_country', :on => :collection
  end
  resources :countries
  resources :patients do
    get 'search_patient', :on => :collection
  end
  resources :patient_types
  resources :addresses
  devise_for :users, :controllers => { :registrations => "users" }
  resources  :users

  root  'welcome#index'

  get   'profile' => 'users#profile', as: :user_profile
  match 'profile' => 'users#update', via: [:put, :patch]

end
