# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160118000323) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "street",      limit: 100
    t.string   "number",      limit: 10
    t.string   "colony",      limit: 50
    t.integer  "city_id"
    t.string   "postal_code", limit: 20
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["country_id"], name: "index_cities_on_country_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_consultations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patient_id"
    t.string   "endocrine"
    t.string   "hypertension"
    t.string   "heart_disease"
    t.string   "tuberculosis"
    t.string   "asthmas_and_allergies"
    t.string   "HIV"
    t.string   "tumor"
    t.string   "other"
    t.string   "prenatal_maternal_age"
    t.string   "prenatal_number_of_deeds"
    t.string   "prenatal_product_deed_number"
    t.string   "prenatal_living"
    t.string   "prenatal_antenatal"
    t.string   "prenatal_complications"
    t.text     "prenatal_which_complications"
    t.string   "natal_natal"
    t.string   "natal_weeks_gestation"
    t.string   "natal_apgar"
    t.string   "natal_weight"
    t.string   "natal_size"
    t.string   "natal_head_circumference"
    t.string   "natal_hospitable"
    t.text     "natal_complications"
    t.string   "postnatal_feeding"
    t.string   "postnatal_supply_amount"
    t.string   "postnatal_number"
    t.string   "postnatal_time"
    t.string   "postnatal_reason"
    t.string   "postnatal_weaning_start"
    t.string   "postnatal_weaning_start_type"
    t.string   "postnatal_current_supply"
    t.string   "postnatal_vaccines_bcg"
    t.string   "postnatal_polio"
    t.string   "postnatal_dpt"
    t.string   "postnatal_hib"
    t.string   "postnatal_hepatitis_b"
    t.string   "postnatal_mmr"
    t.string   "postnatal_hepatitis_a"
    t.string   "postnatal_antivaricela"
    t.string   "postnatal_pneumococcus"
    t.string   "postnatal_menarche"
    t.string   "postnatal_rhythm"
    t.string   "neurological_stare"
    t.string   "neurological_sat_dawn"
    t.string   "neurological_rolled"
    t.string   "neurological_path"
    t.string   "neurological_monosyllables"
    t.string   "smoking"
    t.string   "alcoholism"
    t.string   "drug_addiction"
    t.string   "seizures"
    t.string   "asthma"
    t.string   "allergies"
    t.string   "surgical"
    t.string   "transfusions"
    t.string   "hospitalizations"
    t.string   "measles"
    t.string   "rubella"
    t.string   "parotitis"
    t.string   "chickenpox"
    t.string   "congenital_malformations"
    t.text     "medical_history_comments"
    t.text     "neurological_system"
    t.text     "cardiovascular_system"
    t.text     "respiratory_system"
    t.text     "digestive_apparatus"
    t.text     "genitourinary_system"
    t.text     "musculoskeletal"
    t.text     "auditory"
    t.text     "visual"
    t.text     "systems_and_apparatus_comments"
    t.string   "physical_exploration_weight"
    t.string   "physical_exploration_size"
    t.string   "physical_exploration_per_cef"
    t.string   "physical_exploration_temp"
    t.string   "physical_exploration_fc"
    t.string   "physical_exploration_fr"
    t.string   "physical_exploration_ta"
    t.text     "physical_exploration_general_inspection"
    t.text     "physical_exploration_head"
    t.text     "physical_exploration_neck"
    t.text     "physical_exploration_chest"
    t.text     "physical_exploration_abdomen"
    t.text     "physical_exploration_genitals"
    t.text     "physical_exploration_upper_extremities"
    t.text     "physical_exploration_lower_extremities"
    t.text     "physical_exploration_skin"
    t.text     "physical_exploration_comments"
    t.text     "physical_exploration_diagnosis"
    t.text     "physical_exploration_studies_requested"
    t.text     "physical_exploration_treatment"
    t.text     "physical_exploration_prognostic"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "endocrine_text"
    t.string   "hypertension_text"
    t.string   "heart_disease_text"
    t.string   "tuberculosis_text"
    t.string   "asthmas_and_allergies_text"
    t.string   "tumor_text"
    t.string   "HIV_text"
    t.string   "other_text"
    t.text     "interrogation"
    t.text     "diagnosis"
    t.text     "treatment"
  end

  add_index "medical_consultations", ["patient_id"], name: "index_medical_consultations_on_patient_id", using: :btree
  add_index "medical_consultations", ["user_id"], name: "index_medical_consultations_on_user_id", using: :btree

  create_table "patient_types", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "first_name",         limit: 100
    t.string   "last_name",          limit: 100
    t.string   "full_name",          limit: 100
    t.string   "gender",             limit: 20
    t.date     "birth_date"
    t.text     "birthplace"
    t.string   "street",             limit: 100
    t.string   "number",             limit: 10
    t.string   "colony",             limit: 50
    t.string   "postal_code",        limit: 20
    t.string   "fiscal_street",      limit: 100
    t.string   "fiscal_number",      limit: 10
    t.string   "fiscal_colony",      limit: 50
    t.string   "fiscal_postal_code", limit: 20
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "patient_type_id"
    t.integer  "user_id"
    t.integer  "address_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "patients", ["address_id"], name: "index_patients_on_address_id", using: :btree
  add_index "patients", ["city_id"], name: "index_patients_on_city_id", using: :btree
  add_index "patients", ["country_id"], name: "index_patients_on_country_id", using: :btree
  add_index "patients", ["patient_type_id"], name: "index_patients_on_patient_type_id", using: :btree
  add_index "patients", ["user_id"], name: "index_patients_on_user_id", using: :btree

  create_table "tutors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "occupation", limit: 80
    t.string   "rfc",        limit: 50
    t.string   "phone",      limit: 20
    t.string   "cell_phone", limit: 30
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "patient"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "addresses", "cities"
  add_foreign_key "cities", "countries"
  add_foreign_key "medical_consultations", "patients"
  add_foreign_key "medical_consultations", "users"
  add_foreign_key "patients", "addresses"
  add_foreign_key "patients", "cities"
  add_foreign_key "patients", "countries"
  add_foreign_key "patients", "patient_types"
  add_foreign_key "patients", "users"
end
