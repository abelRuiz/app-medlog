class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :first_name
      t.string :last_name
      t.string :occupation, limit: 80
      t.string :rfc, limit: 50
      t.string :phone, limit: 20
      t.string :cell_phone, limit: 30

      t.timestamps null: false
    end
  end
end
