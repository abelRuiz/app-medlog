class AddColumnsToMedicalConsultation < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :endocrine_text, :string
    add_column :medical_consultations, :hypertension_text, :string
    add_column :medical_consultations, :heart_disease_text, :string
    add_column :medical_consultations, :tuberculosis_text, :string
    add_column :medical_consultations, :asthmas_and_allergies_text, :string
    add_column :medical_consultations, :tumor_text, :string
    add_column :medical_consultations, :HIV_text, :string
    add_column :medical_consultations, :other_text, :string
  end
end
