class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name, limit: 100
      t.string :last_name, limit: 100
      t.string :full_name, limit: 100
      t.string :gender, limit: 20
      t.date :birth_date
      t.text :birthplace, limit: 200

      t.string :street, limit: 100
      t.string :number, limit: 50
      t.string :colony, limit: 50
      t.string :postal_code, limit: 20

      t.string :fiscal_street, limit: 100
      t.string :fiscal_number, limit: 50
      t.string :fiscal_colony, limit: 50
      t.string :fiscal_postal_code, limit: 20

      #t.references :tutor, index: true, foreign_key: true
      t.references :country, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.references :patient_type, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :address, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
