class AddPatientIdToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :patient, :integer
  end
end
