class AddInterrogationDiagnosisTreatmentToMedicalConsultation < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :interrogation, :text
    add_column :medical_consultations, :diagnosis, :text
    add_column :medical_consultations, :treatment, :text
  end
end
