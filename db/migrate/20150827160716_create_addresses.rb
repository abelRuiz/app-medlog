class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street, limit: 100
      t.string :number, limit: 10
      t.string :colony, limit: 50
      t.references :city, index: true, foreign_key: true
      t.string :postal_code, limit: 20

      t.timestamps null: false
    end
  end
end
